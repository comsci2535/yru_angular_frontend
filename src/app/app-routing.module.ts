import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackAccountsAddComponent } from './page/back_accounts/back-accounts-add/back-accounts-add.component';
import { BackAccountsEditComponent } from './page/back_accounts/back-accounts-edit/back-accounts-edit.component';
import { BackAccountsListComponent } from './page/back_accounts/back-accounts-list/back-accounts-list.component';
import { BankHistoricalListComponent } from './page/bank_historical/bank-historical-list/bank-historical-list.component';
import { HomeComponent } from './page/home/home.component';

const routes: Routes = [{
  path:"",
  component:HomeComponent,
  children:[{
    path:"home",
    component: HomeComponent,
  }]
},{
  path:"back_accounts",
  component:BackAccountsListComponent
},{
  path:"back_accounts/add",
  component:BackAccountsAddComponent
},{
  path:"back_accounts/edit/:id",
  component:BackAccountsEditComponent
},{
  path:"bank_historical",
  component:BankHistoricalListComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
