import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackAccountsService {

  baseUrl:string = 'http://localhost:8000/api';
  constructor(private http: HttpClient) {}

  getAll(){
 
    const headers = new HttpHeaders({
      'Content-Type': 'application/json', 
    });

    let params = new HttpParams();
    return this.http.get<any>(this.baseUrl.concat(`/back_account`), { headers, params })
      .pipe(finalize(() => { }));
  }

  getByKey(id:any){
 
    const headers = new HttpHeaders({
      'Content-Type': 'application/json', 
    });

    let params = new HttpParams();
    return this.http.get<any>(this.baseUrl.concat(`/back_account/`+id), { headers, params })
      .pipe(finalize(() => { }));
  }


  getPost(data:any){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const payload = {
      back_code : data.back_code,
      price: data.price,
      back_name : data.back_name
    };

    return this.http.post<any>(this.baseUrl.concat(`/back_account`), payload, { observe: 'response', headers })
      .pipe(finalize(() => { }));
  }

  getPut(data:any){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const payload = {
      back_code : data.back_code,
      price: data.price,
      back_name : data.back_name
    };

    return this.http.put<any>(this.baseUrl.concat(`/back_account/`+data.id), payload, { observe: 'response', headers })
      .pipe(finalize(() => { }));
  }

  delete(id:any){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.delete(this.baseUrl.concat(`/back_account`)+'/'+id, {headers})
    .pipe(finalize(() => { }));
  }
}
