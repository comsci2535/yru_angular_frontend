import { TestBed } from '@angular/core/testing';

import { BankHistoricalService } from './bank-historical.service';

describe('BankHistoricalService', () => {
  let service: BankHistoricalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BankHistoricalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
