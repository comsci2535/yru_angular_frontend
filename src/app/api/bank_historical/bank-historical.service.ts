import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class BankHistoricalService {

  baseUrl:string = 'http://localhost:8000/api';
  constructor(private http: HttpClient) {}

  getAll(){
 
    const headers = new HttpHeaders({
      'Content-Type': 'application/json', 
    });

    let params = new HttpParams();
    return this.http.get<any>(this.baseUrl.concat(`/back_account`), { headers, params })
      .pipe(finalize(() => { }));
  }
  
}
