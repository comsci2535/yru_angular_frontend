import { TestBed } from '@angular/core/testing';

import { BackAccountsService } from './back-accounts.service';

describe('BackAccountsService', () => {
  let service: BackAccountsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackAccountsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
