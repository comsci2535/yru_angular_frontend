import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { MenuBarComponent } from './layout/menu-bar/menu-bar.component';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component'; 
import { BackAccountsAddComponent } from './page/back_accounts/back-accounts-add/back-accounts-add.component';
import { BackAccountsEditComponent } from './page/back_accounts/back-accounts-edit/back-accounts-edit.component';
import { BackAccountsListComponent } from './page/back_accounts/back-accounts-list/back-accounts-list.component'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BankHistoricalListComponent } from './page/bank_historical/bank-historical-list/bank-historical-list.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    MenuBarComponent,
    BreadcrumbComponent,
    BackAccountsAddComponent,
    BackAccountsEditComponent,
    BackAccountsListComponent,
    BankHistoricalListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
