import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankHistoricalListComponent } from './bank-historical-list.component';

describe('BankHistoricalListComponent', () => {
  let component: BankHistoricalListComponent;
  let fixture: ComponentFixture<BankHistoricalListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankHistoricalListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankHistoricalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
