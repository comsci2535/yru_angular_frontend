import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackAccountsService } from 'src/app/api/back-accounts.service';
import swal from "sweetalert2";
@Component({
  selector: 'app-back-accounts-add',
  templateUrl: './back-accounts-add.component.html',
  styleUrls: ['./back-accounts-add.component.css']
})
export class BackAccountsAddComponent implements OnInit {

  constructor(private backAccountsService:BackAccountsService,
    private router:Router) { }

  ngOnInit(): void {
  }

  formSubmitData(form:any){
    console.log(form.value);
    this.backAccountsService.getPost(form.value).toPromise().then(result => {
      console.log(result)
      swal.fire({
        title: 'บันทึกข้อมูลเรียบร้อยแล้ว', 
        showCancelButton: false, 
        icon: 'success',
        confirmButtonColor: "#00D1B2",
        confirmButtonText: 'ตกลง'
    }).then(result => {
        if (result.value) {
          this.router.navigate(['/back_accounts']);
        }
    })  
    })
  }

}
