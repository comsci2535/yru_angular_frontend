import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackAccountsAddComponent } from './back-accounts-add.component';

describe('BackAccountsAddComponent', () => {
  let component: BackAccountsAddComponent;
  let fixture: ComponentFixture<BackAccountsAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackAccountsAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackAccountsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
