import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BackAccountsService } from 'src/app/api/back-accounts.service';
import swal from "sweetalert2";

@Component({
  selector: 'app-back-accounts-list',
  templateUrl: './back-accounts-list.component.html',
  styleUrls: ['./back-accounts-list.component.css']
})
export class BackAccountsListComponent implements OnInit {
  data:any;
  id:any;
  constructor(
    private backAccountsService:BackAccountsService,
    private route:ActivatedRoute) 
    {  } 

  ngOnInit(): void {
    this.getBackAccountsService();
  }

  private async getBackAccountsService(){
    await this.backAccountsService.getAll().toPromise().then(result => {
      console.log(result)
      this.data = result.data;
    })
  }

  btnDelete(data:any){
    console.log(data);
    swal.fire({
      title: 'ต้องการลบข้อมูลหรือไม่ ?', 
      showCancelButton: true, 
      cancelButtonColor: "#C4C4C4",
      confirmButtonColor: "#00D1B2",
      confirmButtonText: 'ตกลง',
      cancelButtonText: 'ยกเลิก'
    }).then(result => {
      if (result.value) {
        this.backAccountsService.delete(data.id).toPromise().then(result => {
          console.log(result)
          
          swal.fire({
            title: 'บันทึกข้อมูลเรียบร้อยแล้ว', 
            showCancelButton: false, 
            icon: 'success',
            confirmButtonColor: "#00D1B2",
            confirmButtonText: 'ตกลง'
        }).then(result => {
            if (result.value) {
              this.getBackAccountsService(); 
            }
        }) 

        })
      }
    });
  }
    
}
