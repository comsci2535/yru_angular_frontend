import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackAccountsListComponent } from './back-accounts-list.component';

describe('BackAccountsListComponent', () => {
  let component: BackAccountsListComponent;
  let fixture: ComponentFixture<BackAccountsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackAccountsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackAccountsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
