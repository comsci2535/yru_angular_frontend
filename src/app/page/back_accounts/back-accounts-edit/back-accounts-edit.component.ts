import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackAccountsService } from 'src/app/api/back-accounts.service';
import swal from "sweetalert2";
@Component({
  selector: 'app-back-accounts-edit',
  templateUrl: './back-accounts-edit.component.html',
  styleUrls: ['./back-accounts-edit.component.css']
})
export class BackAccountsEditComponent implements OnInit {
  data:any;
  id:any;
  constructor( 
    private backAccountsService:BackAccountsService,
    private route:ActivatedRoute,
    private router:Router) {
      this.route.params.subscribe(params => { 
        this.id = params.id;
      })

      this.data = {
        back_name:'',
        price:'',
        back_code:''
      };
      
     }

  ngOnInit(): void {
    this.getBackAccountsService(this.id);
  }

  async getBackAccountsService(id:any){
    await this.backAccountsService.getByKey(id).toPromise().then(result => {
      console.log(result)
      this.data = result.data;
    })
  }



  formSubmitData(form:any){
    console.log(form.value);
    this.backAccountsService.getPut(form.value).toPromise().then(result => {
      console.log(result)
      swal.fire({
          title: 'บันทึกข้อมูลเรียบร้อยแล้ว', 
          showCancelButton: false, 
          icon: 'success',
          confirmButtonColor: "#00D1B2",
          confirmButtonText: 'ตกลง'
      }).then(result => {
          if (result.value) {
            this.router.navigate(['/back_accounts']);
          }
      }) 
      
    })
  }

}
