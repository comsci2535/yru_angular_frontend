import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackAccountsEditComponent } from './back-accounts-edit.component';

describe('BackAccountsEditComponent', () => {
  let component: BackAccountsEditComponent;
  let fixture: ComponentFixture<BackAccountsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackAccountsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackAccountsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
